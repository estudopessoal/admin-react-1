import React, { Component } from 'react';
import Title from '../../components/Title/Title'

export default class Home extends Component {

  constructor() {
    super()
    this.state = {
      title: 'Home'
    }
  }

  render () {
    return (
      <div className="home">
        <Title text={this.state.title}/>
      </div>
    );
  }
}