import React, { Component } from 'react'
import Title from '../../components/Title/Title'
import FormLivro from '../../components/FormLivro/FormLivro'
import './NovoLivro.css'

export default class Content extends Component {
  
  constructor() {
    super()
    this.state = {
      title: 'Novo Livro'
    }
  }

  render () {
    return (
      <div className="novoLivro">
        <Title text={this.state.title}/>
        <FormLivro/>
      </div>
    )
  }
}