import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Title from '../../components/Title/Title'
import './Livros.css'

export default class Livros extends Component {

  constructor() {
    super()
    this.state = {
      title: 'Livros',
      livros: []
    }
  }

  componentDidMount() {
    fetch('/api/livros.json', {method:'GET'})
      .then(res => res.json())
      .then(function(livros){ 
        this.setState({livros})
      }.bind(this))
  }

  render () {
    return (
      <div className="Livros">
        <div className="row">
          <div className="col">
            <Title text={this.state.title}/>
          </div>
          <div className="col d-flex justify-content-end">
            <Link to="/novolivro">
              <button className="btn btn-primary">Adiciona Livro</button>
            </Link>
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Sinopse</th>
              <th>Autor</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.livros.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item.nome}</td>
                    <th>{item.sinopse}</th>
                    <td>{item.autor}</td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}