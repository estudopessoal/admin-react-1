import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'
import Sidebar from '../../components/Sidebar/Sidebar'
import Content from '../../components/Content/Content'

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Sidebar/>
          <Content/>
        </div>
      </BrowserRouter>
    )
  }
}