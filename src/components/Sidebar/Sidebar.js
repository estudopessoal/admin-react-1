import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Sidebar.css'

export default class Sidebar extends Component {
  render () {
    return (
      <div className="sidebar">
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/livros">Livros</Link></li>
        </ul>
      </div>
    )
  }
}