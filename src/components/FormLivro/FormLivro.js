import React, { Component } from 'react'
import './FormLivro.css'

export default class Content extends Component {

  constructor() {
    super()
    this.state = {
      titulo: '',
      sinopse: ''
    }
  }

  envia(e) {
    e.preventDefault()
    const titulo = e.target.titulo
    const sinopse = e.target.sinopse

    console.log(titulo.value)
    console.log(sinopse.value)

    titulo.value = ''
    sinopse.value = ''
  }

  handleChange(e) {
    this.setState({[e.target.name]: e.target.value})
  }

  render () {
    return (
      <form onSubmit={this.envia.bind(this)}>
        <div className="form-group">
          <label>Título</label>
          <input name="titulo" type="text" className="form-control" placeholder="Lorem ipsum dolor" onChange={this.handleChange.bind(this)} value={this.state.titulo}/>
        </div>
        <div className="form-group">
          <label>Sinopse</label>
          <textarea name="sinopse" className="form-control" onChange={this.handleChange.bind(this)} value={this.state.sinopse}></textarea>
        </div>
        <button className="btn btn-primary">Cadastrar</button>
      </form>
    )
  }
}