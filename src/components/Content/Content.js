import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'

import Home from '../../containers/Home/Home'
import Livros from '../../containers/Livros/Livros'
import NovoLivro from '../../containers/NovoLivro/NovoLivro'

import './Content.css'

export default class Content extends Component {
  
  render () {
    return (
      <div className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/novolivro' component={NovoLivro}/>
                <Route path='/livros' component={Livros}/>
              </Switch>
            </div>
          </div>
        </div>
      </div>
    )
  }
}