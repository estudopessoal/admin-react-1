import React, { Component } from 'react'
import glamorous from 'glamorous'

export default class Title extends Component {
  render () {
    return (
      <MyTitle>{this.props.text}</MyTitle>
    )
  }
}

const MyTitle = glamorous.h1({
  marginBottom: '40px'
})